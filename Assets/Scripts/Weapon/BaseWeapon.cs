using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseWeapon : MonoBehaviour
{
    public static event Action ReloadAction = delegate { };
    public static event Action ReloadActionEnd = delegate { };
    public static event Action ShootingAction = delegate { };
    public string NameWeapon = "Awesome Weapon";
    
    public float ReloadTime = 0;
    public float ShootingInterval = 0.2f;

    public int PatronsCount = 5; //� ������
    public int MaxPatronsCount = 5;

    public int PatronsInClip = 15; //� ������
    public int MaxPatronsInClip = 15;

    public bool isReloading = false;

    public int WeaponDamage = 10;

    public Sprite weaponIcon;
    public virtual void Start()
    {
        PatronsCount = MaxPatronsCount;
        PatronsInClip = MaxPatronsInClip;
        isReloading = false;
    }
    public virtual void Reload()
    {
        if (PatronsCount <= 0) return;
        ReloadAction();
    }

    public virtual void ReloadEnd()
    {
        int petronsToReload = MaxPatronsInClip - PatronsInClip;
        if (PatronsCount - petronsToReload >= 0)
        {
            PatronsInClip = MaxPatronsInClip;
            PatronsCount -= petronsToReload;
        }
        else
        {
            PatronsInClip += PatronsCount;
            PatronsCount = 0;
        }
        isReloading = false;
        UIManager.instance.UpdateAmmo();
        ReloadActionEnd();
    }

    public virtual void Shoot()
    {
        PatronsInClip--;
        ShootingAction();
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit))
        {
            IDamageable myDamageable = hit.transform.gameObject.GetComponent<IDamageable>();
            if (myDamageable != null)
                myDamageable.TakeDamage(WeaponDamage);
        }
        if (PatronsInClip == 0)
        {
            isReloading = true;
            Reload();
        }
    }

    //line renderer ����������� ��������, ������� ������� ������,  �������, � ������� ������ � � �����(������) 
    //��� �������� ������� raycast �� ������ � �� ������, ��� �����������, ��� � ��������

    public virtual void OnUpdate()
    {
        print("I am Update");
    }
    //��� ������������� �������� ��������� ���������� isshooting
    
}
