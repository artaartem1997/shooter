using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RegionListing : MonoBehaviour
{
    public string RegionToken;
    [SerializeField]
    private Text RegionName;

    public void SetRegion()
    {
        PlayerPrefs.SetString("Region", RegionToken);
        RegionListingMenu.MyInstance.OnRegionChanged(RegionToken);
    }
}
