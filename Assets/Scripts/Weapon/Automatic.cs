using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
public class Automatic : BaseWeapon
{
    [SerializeField]private float nextTimeToFire = 0f;
    private void Start()
    {
        nextTimeToFire = 0;
    }
    public override void OnUpdate()
    {
        if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire && !isReloading)
        {
            if (PatronsInClip > 0)
            {
                Shoot();
                nextTimeToFire = Time.time + ShootingInterval;
            }
        }

        if (Input.GetKeyDown(KeyCode.R) && (PatronsInClip!=MaxPatronsInClip))
        {
            isReloading = true;
            Reload();
        }
    }
}

//���������� ���� ���������� isShooting � ��������� �� ��� ���� ��������
