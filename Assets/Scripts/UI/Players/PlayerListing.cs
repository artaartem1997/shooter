using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerListing : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private Text _text;
    public Player Player { get; private set; }
    public bool Ready = false;
    public Image ReadyImage;
    public void SetPlayerInfo(Player player)
    {
        Player = player;

        SetPlayerText(player);
    }

    private void SetPlayerText(Player player)
    {
        int result = -1;
        _text.text = result.ToString() + ", " + player.NickName;
    }
}
