using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour
{
    [SerializeField] private InputField NickNameInput;
    public void OnClick_Connect()
    {
        if (NickNameInput.text != "")
        {
            PlayerPrefs.SetString("NickName", NickNameInput.text);
        }
        SceneManager.LoadScene("Rooms", LoadSceneMode.Single);
    }

    public void OnClick_Exit()
    {
        Application.Quit();
    }
}
