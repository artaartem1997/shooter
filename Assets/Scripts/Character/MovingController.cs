using ExitGames.Client.Photon.StructWrapping;
using Photon.Pun;
using UnityEngine;

public class MovingController : MonoBehaviour
{    
    private CharacterController controller;
    private Vector3 VerticalMovement;
    [SerializeField]private float jumpHeight = 2.5f;
    private float gravity = 9.81f;
    private PhotonView photonView;
    public Vector3 previousPosition = Vector3.zero;
    public Vector3 currentPosition = Vector3.zero;
    private float speed = 6;

    public float XBias, ZBias;

    private void Start()
    {
        photonView = GetComponent<PhotonView>();
        controller = GetComponent<CharacterController>();
        //if (photonView.IsMine)
            CameraController.instance.target = transform;
    }
    private void Jump()
    {

        #region ������
        if (controller.isGrounded)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                VerticalMovement.y = jumpHeight;
                //animator.JumpAnimation();
            }
        }
        else
        {
            VerticalMovement.y -= gravity * Time.deltaTime;
        }
        #endregion

        controller.Move(VerticalMovement * Time.deltaTime);

    }





    private void Update()
    {
        //if (photonView.IsMine && !GetComponent<Character>().isDie)
        {
            float horizontal = Input.GetAxisRaw("Horizontal");
            float vertical = Input.GetAxisRaw("Vertical");
            Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;

            if (direction.magnitude >= 0.1f)
            {
                var moveDirection = transform.TransformDirection(new Vector3(horizontal, 0, vertical)) * speed;
                controller.SimpleMove(moveDirection);
            }

            Jump();
        }
    }

    private void LateUpdate()
    {
        //if (photonView.IsMine && !GetComponent<Character>().isDie)
        {
            transform.forward = CameraController.instance.camPivotY.forward;
        }        
    }
}
