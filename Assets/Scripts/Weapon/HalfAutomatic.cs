using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HalfAutomatic : BaseWeapon
{

    public override void OnUpdate()
    {
        if (Input.GetButtonDown("Fire1") && !isReloading)
        {
            if (PatronsInClip > 0)
            {
                Shoot();                
            }
        }

        if (Input.GetKeyDown(KeyCode.R) && (PatronsInClip != MaxPatronsInClip))
        {
            isReloading = true;
            Reload();
        }
    }
}
