using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    public static WeaponManager instance;
    public BaseWeapon currentWeapon;
    public List<BaseWeapon> myWeapons = new List<BaseWeapon>();
    private int weaponID = 0;
    public Transform weaponPoint;
    public bool isShooting = false;
    public int MyWeaponID
    {
        get
        {
            return weaponID;
        }
        set
        {
            weaponID = value;
            if (weaponID == -1) weaponID = myWeapons.Count - 1;
            if (weaponID == myWeapons.Count) weaponID = 0;
        }
    }
    private void Awake()
    {
        instance = this;
    }
    private void Update()
    {
        currentWeapon.OnUpdate();
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        if (myWeapons.Count > 0 && scroll!=0)
        {
            ChangeWeapon(scroll);
        }
    }
    public void InitializeWeapon()
    {
        if (weaponPoint.childCount>0) Destroy(weaponPoint.GetChild(0).gameObject);
        GameObject newAmmo = Instantiate(currentWeapon.gameObject, weaponPoint);
        newAmmo.transform.position = weaponPoint.position;
    }
    public void ChangeWeapon(float scroll)
    {
        if (scroll < 0)
        {
            MyWeaponID--;
        }
        else
        {
            MyWeaponID++;
        }
        currentWeapon = myWeapons[MyWeaponID];
        InitializeWeapon();
        UIManager.instance.OnChangeAmmo();
    }
}
