using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviourPun
{
    private Animator animator;
    private MovingController controller;
    private float ZBias, XBias;
    private bool isReloading = false;
    private bool isShootingAction = false;

    private void Start()
    {
        animator = GetComponent<Animator>();
        controller = GetComponent<MovingController>();

        BaseWeapon.ReloadAction += SetReloadAnimation;
        BaseWeapon.ReloadActionEnd += StopReloadAnimation;
    }
    private void OnDestroy()
    {
        BaseWeapon.ReloadAction -= SetReloadAnimation;
        BaseWeapon.ReloadActionEnd -= StopReloadAnimation;
    }
    //������� ����� 
    private void SetReloadAnimation()
    {
        isReloading = true;
        isShootingAction = true;
        animator.SetBool("Reload", isReloading);
    }
    private void StopReloadAnimation()
    {
        isReloading = false;
        isShootingAction = false;
        animator.SetBool("Reload", isReloading);
    }
    private void LateUpdate()
    {
        if (photonView.IsMine)
        {
            LayerManagement();
            controller.currentPosition = transform.position;

            Vector3 positionBias = (controller.currentPosition - controller.previousPosition).normalized;

            ZBias = Vector3.Dot(transform.forward, positionBias);
            XBias = Vector3.Dot(transform.right, positionBias);

            controller.previousPosition = controller.currentPosition;
            animator.SetFloat("Xbias", XBias);
            animator.SetFloat("Zbias", ZBias);
            if (GetComponent<Character>().isDie)
            { 
                animator.SetBool("Death", true);
            }
            else
            {
                animator.SetBool("Death", false);
            }
        }
    }

    private void LayerManagement()
    {
        if (isShootingAction)
        {
            ActivateLayer("Shooting");
        }
        else if (GetComponent<Character>().isDie)
        {
            ActivateLayer("Death");
        }
        else
        {
            ActivateLayer("Moving");
        }
    }

    public void ActivateLayer(string nameLayer)
    {
        for (int i = 0; i < animator.layerCount; i++)
        {
            animator.SetLayerWeight(i, 0);
        }
        animator.SetLayerWeight(animator.GetLayerIndex(nameLayer), 1);
        animator.SetLayerWeight(animator.GetLayerIndex("Shooting"), 1);
    }
}
