using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomListing : MonoBehaviour
{
    [SerializeField] private Text roomName;
    public RoomInfo roomInfo { get; private set; }


    public void SetRoomInfo(RoomInfo roominfo)
    {
        roomInfo = roominfo;
        roomName.text = roominfo.Name + " " + roominfo.PlayerCount + "/" + roominfo.MaxPlayers;
    }

    public void OnClick_EnterRoom()
    {
        if (roomInfo.PlayerCount < roomInfo.MaxPlayers)
        {
            PhotonNetwork.JoinRoom(roomInfo.Name);
        }
    }
}
