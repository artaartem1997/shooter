using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour, IDamageable
{
    [SerializeField]
    private int health;
    public int MyHealth
    {
        get
        {
            return health;
        }
        set
        {
            health = value;
            if (health <= 0) Die();
        }
    }
    public void TakeDamage(int damage)
    {
        MyHealth -= damage;
    }

    private void Die()
    {
        Destroy(gameObject);
    }
}
