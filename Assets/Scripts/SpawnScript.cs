using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpawnScript : MonoBehaviourPunCallbacks
{
    public static SpawnScript instance;

    [SerializeField]
    private Transform startPositionPoints;

    private void Awake()
    {
        instance = this;
        PhotonNetwork.Instantiate("Player", startPositionPoints.GetChild(Random.Range(0, startPositionPoints.childCount)).position, Quaternion.identity); //���������� 
    }

    //allviaserver ��� �������� �������, ����� ������������ �������� ������ �� ���� ������� ���������

    public void Respawn(Transform player)
    {
        player.position = startPositionPoints.GetChild(Random.Range(0, startPositionPoints.childCount)).position;
    }
}
