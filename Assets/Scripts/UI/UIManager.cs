using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;
    public Character character;

    [SerializeField] private Text healphText;
    [SerializeField] private Text patronsText;
    [SerializeField] private Image AmmoImage;
    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        BaseWeapon.ShootingAction += UpdateAmmo;
    }
    public void FirstInitialize()
    {
        OnChangeAmmo();
        UpdateHealph();
    }
    public void UpdateHealph()
    {
        healphText.text = character.MyHealth.ToString();
    }

    public void UpdateAmmo()
    {
        patronsText.text = WeaponManager.instance.currentWeapon.PatronsInClip + "/" + WeaponManager.instance.currentWeapon.PatronsCount;        
    }

    public void OnChangeAmmo()
    {
        patronsText.text = WeaponManager.instance.currentWeapon.PatronsInClip + "/" + WeaponManager.instance.currentWeapon.PatronsCount;
        AmmoImage.sprite = WeaponManager.instance.currentWeapon.weaponIcon;
    }

    private void OnDestroy()
    {
        BaseWeapon.ShootingAction -= UpdateAmmo;
    }
}
