using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RegionListingMenu : MonoBehaviour
{
    [SerializeField]
    private Transform _content;
    private List<RegionListing> regionsListing = new List<RegionListing>();

    private static RegionListingMenu instance;

    public static RegionListingMenu MyInstance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<RegionListingMenu>();
            }
            return instance;
        }
    }

    private void Start()
    {
        foreach (Transform child in _content)
        {
            regionsListing.Add(child.gameObject.GetComponent<RegionListing>());
        }
        regionsListing[0].SetRegion();
    }

    public void OnRegionChanged(string regtoken)
    {
        foreach (RegionListing listing in regionsListing)
        {
            listing.gameObject.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1);
            if (listing.RegionToken == regtoken)
            {
                listing.gameObject.GetComponent<Image>().color = new Color(0.526852f, 1f, 0.4292453f, 1);
            }
        }
    }
}
