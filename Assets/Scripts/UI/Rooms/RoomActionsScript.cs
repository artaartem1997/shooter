using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Realtime;

public class RoomActionsScript : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private Text _roomName;
    [SerializeField]
    private RoomListingMenu roomListingsMenu;
    [SerializeField]
    private GameObject PlayerListingCanvas, RoomListingCanvas;
    [SerializeField]
    private PlayerListingMenu playerListingMenu;
    [SerializeField]
    private RoomListingMenu roomListingMenu;


    public override void OnJoinedRoom()
    {
        RoomListingCanvas.SetActive(false);
        PlayerListingCanvas.SetActive(true);
        foreach (Transform child in roomListingMenu._content)
        {
            Destroy(child.gameObject);
        }
        roomListingMenu._listings.Clear();
    }


    public void OnClick_CreateRoom()
    {
        if (!PhotonNetwork.IsConnected || _roomName.text.Length == 0)
            return;
        RoomOptions options = new RoomOptions();
        options.BroadcastPropsChangeToAll = true;
        options.MaxPlayers = 10;
        options.EmptyRoomTtl = 0;
        options.PlayerTtl = 0;
        PhotonNetwork.CreateRoom(_roomName.text, options, TypedLobby.Default);
    }

    public void OnClick_LeaveRoom()
    {
        PhotonNetwork.LeaveRoom(true);
    }

    public override void OnLeftRoom()
    {
        playerListingMenu._readyUpButton.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1);
        RoomListingCanvas.SetActive(true);
        PlayerListingCanvas.SetActive(false);
    }

    public override void OnCreatedRoom()
    {
        Debug.Log("Created room successfully.");
        PlayerListingCanvas.SetActive(true);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Room creation failed: " + message);
    }
}
