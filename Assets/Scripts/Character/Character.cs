using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour, IDamageable
{
    [SerializeField]
    private float respawnTime = 3;
    public bool isDie = false;

    public Transform WeaponPoint;

    [SerializeField]
    private int maxHealth = 100;
    [SerializeField]
    private int health = 100;
    private PhotonView photonView;
    public int MyHealth
    {
        get
        {
            return health;
        }
        set
        {
            health = value;
            if (health > maxHealth) health = maxHealth;
            if (health <= 0 && !isDie) Die();
        }
    }
    private void Awake()
    {
        photonView = GetComponent<PhotonView>();
        health = maxHealth = 100;
    }
    private void Start()
    {
        //if (photonView.IsMine)
        {
            WeaponManager.instance.weaponPoint = WeaponPoint;
            WeaponManager.instance.InitializeWeapon();
            UIManager.instance.character = this;
            UIManager.instance.FirstInitialize();
            BaseWeapon.ReloadAction += Reload;
            MyHealth = maxHealth;
        }
    }

    private void OnDestroy()
    {
        BaseWeapon.ReloadAction -= Reload;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.T) && !isDie)
        {
            Die();
        }
    }


    public void Die()
    {
        isDie = true;
        StartCoroutine(TimeToRespawn(respawnTime));
    }


    private IEnumerator TimeToRespawn(float time)
    {
        while (time > 0)
        {
            yield return new WaitForEndOfFrame();
            time -= Time.deltaTime;
        }
        isDie = false;
        Respawn();
    }

    public void Respawn()
    {
        SpawnScript.instance.Respawn(transform);
    }


    public void TakeDamage(int damage)
    {
        photonView.RPC("RPC_TakeDamage", RpcTarget.All, damage);
    }

    [PunRPC]
    private void RPC_TakeDamage(int damage)
    {
        //if (photonView.IsMine)
        {
            MyHealth -= damage;
            UIManager.instance.UpdateHealph();
        }
    }
    private void Reload()
    {
        StartCoroutine(ReloadAmmo(WeaponManager.instance.currentWeapon.ReloadTime));
    }
    private IEnumerator ReloadAmmo(float time)
    {        
        yield return new WaitForSeconds(time);
        WeaponManager.instance.currentWeapon.ReloadEnd();
    }
}
