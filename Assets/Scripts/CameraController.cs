using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController instance;

    public Transform target;

    public Transform camPivotY;
    public Transform camPivotX;

    public float sens = 5f;

    void Awake()
    {
        instance = this;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void LateUpdate()
    {
        if (target == null) return;

        transform.position = target.position;

        float y = Input.GetAxisRaw("Mouse X");
        float x = Input.GetAxisRaw("Mouse Y");

        Vector3 localRotPivotY = camPivotY.transform.localRotation.eulerAngles;
        Vector3 localRotPivotX = camPivotX.transform.localRotation.eulerAngles;

        localRotPivotY.y += y * sens;
        localRotPivotX.x -= x * sens;

        localRotPivotY.y = FixAngle(localRotPivotY.y);
        localRotPivotX.x = Mathf.Clamp(FixAngle(localRotPivotX.x), -45f, 45f);

        camPivotY.transform.localRotation = Quaternion.Euler(localRotPivotY);
        camPivotX.transform.localRotation = Quaternion.Euler(localRotPivotX);

    }


    public static float FixAngle(float angle)
    {
        if (angle <= -180)
        {
            angle += 360;
        }
        else if (angle > 180)
        {
            angle -= 360;
        }
        return angle;

    }

    void OnDestroy()
    {
        instance = null;
    }

}
