using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ConnectionScript : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private GameObject ConnectingPanel;
    private string nick = "";

    private void Start()
    {
        ConnectingPanel.SetActive(true);

        PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = PlayerPrefs.GetString("Region");
        PhotonNetwork.SerializationRate = 40;
        PhotonNetwork.AutomaticallySyncScene = true;
        if (PlayerPrefs.HasKey("NickName"))
        {
            nick = PlayerPrefs.GetString("NickName");
            PlayerPrefs.DeleteKey("NickName");
        }
        if (nick == "")
            PhotonNetwork.LocalPlayer.NickName = GenerateNickName();
        else
            PhotonNetwork.LocalPlayer.NickName = nick;
        PhotonNetwork.ConnectUsingSettings();
    }

    private string GenerateNickName()
    {
        return "ShootMaster" + Random.Range(0, 10000);
    }


    public override void OnConnectedToMaster()
    {
        print(PhotonNetwork.LocalPlayer.NickName);
        ConnectingPanel.SetActive(false);
        if (!PhotonNetwork.InLobby)
            PhotonNetwork.JoinLobby();
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        print("Disconnected from server for reason " + cause.ToString());
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }

    public void OnClick_Disconnect()
    {
        PhotonNetwork.Disconnect();
    }
}
